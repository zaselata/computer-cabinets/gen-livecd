# gen-livecd

Скриптове за генериране на LiveCD изображние за работните станции

# Клониране на Git хранилището със събмодулите

```bash
git clone --recurse-submodules https://gitlab.com/zaselata/computer-cabinets/gen-livecd.git

```

## Генериране на iso изображение с [Cubic](https://launchpad.net/cubic)

Лесен, графичен метод. В последствие може да се напише Ansible роля за напълно автоматичен deployment, но засега този метод работи.
Документацията е в [Wiki страницата на проекта](https://gitlab.com/zaselata/computer-cabinets/gen-livecd/wikis/Deployment-with-Cubic) .
